# test
This project goes through all the latest tweets with "golang" word in them and
handle their usernames with github if exists.

It has 2 endpoints:
http://localhost:8080/data - which gives JSON info with all fields.

http://localhost:8080/data/2018-10-15/2018-10-15 - which gives the most popular
twitter and github accounts between this dates.

All configuration data you can find in config.yaml

Also you need is to create postgres database with parameters described in config
.yaml file.
After that you can migrate data to your database use: go run main.go
-MigrationOption=BuildMigration -MigrationFile=filename. File calls migrations.

To build your own migration from your database use: go run main.go
-MigrationOption=CreateMigration -MigrationFile=filename. File name will be
stored in internal/migrations in .json format. After that you can use it to
build migration.

