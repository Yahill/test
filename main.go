package main

import (
	"github.com/spf13/test/internal/config"
	"github.com/spf13/test/internal/migrations"
	"flag"
	"github.com/spf13/test/internal/database"
	"github.com/gorilla/mux"
	"github.com/spf13/test/internal/handler"
	"log"
	"net/http"
)

func main(){
	migrationOption := flag.String("MigrationOption", " ", "Choose option, " +
		"build or create migration.")
	migrationFile := flag.String("MigrationFile", " ",
		"Choose file for migration.")

	flag.Parse()

	//read config
	conf := config.GetConfig("config.yaml")

	switch *migrationOption {
	case "CreateMigration":
		migrations.CreateMigration(*migrationFile)
	case "BuildMigration":
		migrations.BuildMigration(*migrationFile)
	case " ":
		//fill database with info from twitter and github in the goroutine
		go database.WriteIntoDB()
		//create new router
		route := mux.NewRouter()

		route.HandleFunc("/data", handler.GetAllData).Methods("GET")

		route.HandleFunc("/data/{firstdate}/{seconddate}",
			handler.GetDataByDate).Methods("GET")

		log.Fatal(http.ListenAndServe(conf.GetPort(), route))
	}
}
