package migrations

import (
	"github.com/spf13/test/internal/database"
	"fmt"
	"os"
)

func CreateMigration(filename string){
	db := database.GetDB()
	db.GetDataFromDB()
	bytes := db.GetDataJson()
	text := string(bytes)
	text = `{"Users":` + text + "}"

	filePath := "internal/migrations/" + filename + ".json"

	file, err := os.Create(filePath)
	if err != nil{
		fmt.Println(err)
	}
	defer file.Close()

	_, err = file.WriteString(text)
}
