package migrations

import (
	"os"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"github.com/spf13/test/internal/config"
	"database/sql"
	_ "github.com/lib/pq"
)

type query struct{
	Data []data `json:"Users"`
}

type data struct{
	Username string `json:"Username"`
	Date string `json:"Date"`
	TwitterFollowers int `json:"TwitterFollowers"`
	GitHubFollowers int `json:"GitHubFollowers"`
}

func BuildMigration(filename string){
	var query query
	filePath := "internal/migrations/" + filename + ".json"

	jsonFile, err := os.Open(filePath)
	if err != nil{
		fmt.Println("Can't open .json file: ", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	json.Unmarshal(byteValue, &query)

	conf := config.GetConfig("config.yaml")
	//create string fot conection to DB
	host, port, userDB, password, dbname := conf.GetDataBaseCredentials()
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, userDB, password, dbname)

	//Open DB
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println("Can't open Database:", err)
	}
	defer db.Close()

	sqlStatement := `CREATE TABLE IF NOT EXISTS twitter(
	  id SERIAL PRIMARY KEY,
  	  username TEXT,
  	  date date,
  	  twitter_followers INT,
  	  github_followers INT
	)`

	_, err = db.Exec(sqlStatement)
	if err != nil {
		fmt.Println("Can't exec DataBase:", err)
	}

	sqlStatement = `
INSERT INTO twitter (username, date, twitter_followers, github_followers)
VALUES ($1, $2, $3, $4)`

	for i := range query.Data{
		_, err := db.Exec(sqlStatement, query.Data[i].Username,
			query.Data[i].Date, query.Data[i].TwitterFollowers, query.Data[i].GitHubFollowers)
		if err != nil{
			fmt.Println("Can't exec DB: ", err)
		}
	}

	sqlStatement = `DELETE FROM twitter WHERE ctid NOT IN
(SELECT max(ctid) FROM twitter GROUP BY username);`

	_, err = db.Exec(sqlStatement)
	if err != nil {
		fmt.Println("Can't exec DataBase:", err)
	}
}
