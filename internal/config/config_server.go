package config

import (
	"io/ioutil"
	"log"
	"fmt"
	"gopkg.in/yaml.v2"
	"strconv"
)

type Configuration interface{
	GetPort() string
	GetTwitterKeys() (string, string)
	GetDataBaseCredentials()(string, int, string,
		string, string)
}

type configuration struct{
	Server port `yaml:"server"`
	Twitter keysForTwitter `yaml:"twitter"`
	Database credentialsDatabase `yaml:"database"`
}

type port struct{
	Port int `yaml:"port"`
}

type keysForTwitter struct{
	ConsumerKey string `yaml:"consumerKey"`
	ConsumerSecret string `yaml:"consumerSecret"`
}

type credentialsDatabase struct{
	Host string `yaml:"host"`
	Port int `yaml:"port"`
	UserDB string `yaml:"userDB"`
	Password string `yaml:"password"`
	Dbname string `yaml:"dbname"`
}

func GetConfig(path string) Configuration{
	var conf configuration

	yamlFile, err := ioutil.ReadFile(path)
	if err != nil{
		fmt.Println("Error in opening config.yaml file: ", err)
	}

	err = yaml.Unmarshal(yamlFile, &conf)
	if err != nil{
		log.Println("Error in unmarshal config.yaml file: ", err)
	}

	return &configuration{conf.Server, conf.Twitter, conf.Database}
}

func (conf *configuration) GetPort() string{
	var port string

	if conf.Server.Port == 0 || conf.Server.Port < 8000 || conf.Server.Port > 8500{
		fmt.Println("Incorrect port, ", conf.Server.Port,
			"use only port from 8000 to 8500 in config.yaml file.")
	}

	port = ":" + strconv.Itoa(conf.Server.Port)
	fmt.Println("Server uses, this:", port, "port.")

	return port

}

func (conf *configuration) GetTwitterKeys() (string, string){
	return conf.Twitter.ConsumerKey, conf.Twitter.ConsumerSecret
}

func (conf *configuration) GetDataBaseCredentials()(string, int, string,
	string, string){
	return conf.Database.Host, conf.Database.Port, conf.Database.UserDB,
	conf.Database.Password, conf.Database.Dbname
}
