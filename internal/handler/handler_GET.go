package handler

import (
	"github.com/spf13/test/internal/database"
	"net/http"
	"github.com/gorilla/mux"
)

func GetAllData(w http.ResponseWriter, r *http.Request){
	db := database.GetDB()
	//read all files from the DB
	db.GetDataFromDB()

	w.WriteHeader(http.StatusOK)
	//send all data via JSON
	w.Write(db.GetDataJson())
}

func GetDataByDate(w http.ResponseWriter, r *http.Request){
	//read variables with dates from request
	vars := mux.Vars(r)
	firstdate := vars["firstdate"]
	seconddate := vars["seconddate"]

	db := database.GetDB()
	//get all data between requested dates
	db.GetDataByDate(firstdate, seconddate)

	w.WriteHeader(http.StatusOK)
	//send answer for request with the most popular accounts
	w.Write(db.GetMostPopularJson())
}