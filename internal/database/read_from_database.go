package database

import(
	_ "github.com/lib/pq"
	"fmt"
	"database/sql"
	"encoding/json"
	"github.com/spf13/test/internal/config"
)

type DataBasePostgres interface{
	GetDataFromDB()
	GetDataByDate(firstdate, seconddate string)
	GetMostPopularJson() []byte
	GetDataJson() []byte
}

type data struct{
	data []dataRow
}

type dataRow struct{
	Username string
	Date string
	TwitterFollowers int
	GitHubFollowers int
}

func GetDB() DataBasePostgres{
	return &data{}
}

func (dta *data) GetDataFromDB(){
	//read config for DB credentials
	conf := config.GetConfig("config.yaml")
	host, port, userDB, password, dbname := conf.GetDataBaseCredentials()

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, userDB, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println("Can't open Database:", err)
	}
	defer db.Close()

	//collect all data from DB
	rows, err := db.Query("SELECT username, date, twitter_followers, " +
		"github_followers FROM twitter")
	if err != nil {
		fmt.Println("Can't query DataBase:", err)
	}
	defer rows.Close()

	for rows.Next(){
		var buff dataRow
		err := rows.Scan(&buff.Username, &buff.Date, &buff.TwitterFollowers,
			&buff.GitHubFollowers)
		if err != nil{
			fmt.Println("Can't scan Row:", err)
		}
		dta.data = append(dta.data, buff)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("Error in a rows from DataBase:", err)
	}
}

func (dta *data) GetDataByDate(firstdate, seconddate string){
	conf := config.GetConfig("config.yaml")
	host, port, userDB, password, dbname := conf.GetDataBaseCredentials()

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, userDB, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println("Can't open Database:", err)
	}
	defer db.Close()

	//get all data between required dates
	rows, err := db.Query("SELECT username, date, twitter_followers, " +
		"github_followers FROM twitter WHERE date BETWEEN $1 AND $2",
		firstdate, seconddate)
	if err != nil {
		fmt.Println("Can't query DataBase:", err)
	}
	defer rows.Close()

	for rows.Next(){
		var buff dataRow
		err := rows.Scan(&buff.Username, &buff.Date, &buff.TwitterFollowers,
			&buff.GitHubFollowers)
		if err != nil{
			fmt.Println("Can't scan Row:", err)
		}
		dta.data = append(dta.data, buff)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("Error in a rows from DataBase:", err)
	}
}

func (dta *data) GetMostPopularJson() []byte{
	//look for most popular accounts
	var result data
	var buffData dataRow
	buff := 0

	for i := range dta.data{
		if dta.data[i].TwitterFollowers > buff{
			buffData = dta.data[i]
			buff = dta.data[i].TwitterFollowers
		}
	}
	result.data = append(result.data, buffData)

	buff = 0

	for i := range dta.data{
		if dta.data[i].GitHubFollowers > buff{
			buffData = dta.data[i]
			buff = dta.data[i].GitHubFollowers
		}
	}
	result.data = append(result.data, buffData)

	resultJson, err := json.Marshal(result.data)
	if err != nil{
		fmt.Println("Can't Marshal data to JSON", err)
	}

	return resultJson
}

func (dta *data) GetDataJson() []byte{
	//decode data to JSON
	result, err := json.Marshal(dta.data)
	if err != nil{
		fmt.Println("Can't Marshal data to JSON", err)
	}

	return result
}
