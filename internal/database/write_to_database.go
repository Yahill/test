package database

import (
	"github.com/spf13/test/internal/twitter"
	"time"
)

func WriteIntoDB(){
	ticker := time.NewTicker(60 * time.Second)

	for{
		<- ticker.C
		tw := twitter.GetTwitter()
		tw.WriteTwitterDataToDataBase()
	}
}
