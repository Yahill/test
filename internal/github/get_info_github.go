package github

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
)

type GitHubData interface{
	GetGitHubData(id string) int
}

type githubUser struct{
	UserId string `json:"login"`
	Followers int `json:"followers"`
}

func GetGitHub() GitHubData{
	return &githubUser{}
}

func (gt *githubUser) GetGitHubData(id string) int{
	resp := SearchGitHubById(id)

	//decode all data from JSON
	err := json.Unmarshal(resp, gt)
	if err != nil{
		fmt.Println("Can't parse json,", err)
	}

	return gt.Followers
}

func SearchGitHubById(githubId string) []byte{
	//looking for the github accounts with username - "githubId"
	resp, err := http.Get("https://api.github.com/users/" + githubId)
	if err != nil{
		fmt.Println("Can't get github", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	return body
}
