package twitter

import (
	"encoding/json"
	"net/http"
	"log"
	"fmt"
	"io/ioutil"
	"net/url"
	"bytes"
	"strconv"
	b64 "encoding/base64"
	"database/sql"
	_ "github.com/lib/pq"
	"github.com/spf13/test/internal/github"
	"github.com/spf13/test/internal/config"
)

type Twitter interface{
	WriteTwitterDataToDataBase()
}

type tweetsAPIResponse struct{
	Tweet []tweet `json:"statuses"`
}

type tweet struct{
	CreatedAt string `json:"created_at"`
	User user `json:"user"`
}

type user struct{
	UserName string `json:"name"`
	Followers int `json:"followers_count"`
}

func GetTwitter() Twitter{
	return &tweetsAPIResponse{}
}

func (tw *tweetsAPIResponse) WriteTwitterDataToDataBase(){
	//look for the tweets
	resp := SearchTwitterQuery()

	//decode JSON from twitter
	err := json.Unmarshal(resp, tw)
	if err != nil{
		fmt.Println("Can't parse json,", err)
	}

	//read config file for DB credentials
	conf := config.GetConfig("config.yaml")
	//create string fot conection to DB
	host, port, userDB, password, dbname := conf.GetDataBaseCredentials()
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, userDB, password, dbname)

	//Open DB
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println("Can't open Database:", err)
	}
	defer db.Close()

	//write info from twitter and github to DB
	sqlStatement := `
INSERT INTO twitter (username, date, twitter_followers, github_followers)
VALUES ($1, $2, $3, $4)`

	//check for the github accounts with similar ids
	for i := range tw.Tweet{
		gt := github.GetGitHub()
		_, err = db.Exec(sqlStatement, tw.Tweet[i].User.UserName,
			"NOW()", tw.Tweet[i].User.Followers, gt.GetGitHubData(tw.Tweet[i].
				User.UserName))
		if err != nil {
			fmt.Println("Can't exec DataBase:", err)
		}
	}

	//delete duplicates by username from DB
	sqlStatement = `DELETE FROM twitter WHERE ctid NOT IN
(SELECT max(ctid) FROM twitter GROUP BY username);`

	_, err = db.Exec(sqlStatement)
	if err != nil {
		fmt.Println("Can't exec DataBase:", err)
	}
}

func SearchTwitterQuery() []byte{
	client := &http.Client{}

	conf := config.GetConfig("config.yaml")
	consumerKey, consumerSecret := conf.GetTwitterKeys()
	//Step 1: Encode consumer key and secret
	encodedKeySecret := b64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s",
		url.QueryEscape(consumerKey),
		url.QueryEscape(consumerSecret))))

	//Step 2: Obtain a bearer token
	//The body of the request must be grant_type=client_credentials
	reqBody := bytes.NewBuffer([]byte(`grant_type=client_credentials`))
	//The request must be a HTTP POST request
	req, err := http.NewRequest("POST", "https://api.twitter.com/oauth2/token", reqBody)
	if err != nil {
		log.Fatal(err, client, req)
	}
	//The request must include an Authorization header formatted as
	//Basic <base64 encoded value from step 1>.
	req.Header.Add("Authorization", fmt.Sprintf("Basic %s", encodedKeySecret))
	//The request must include a Content-Type header with
	//the value of application/x-www-form-urlencoded;charset=UTF-8.
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	req.Header.Add("Content-Length", strconv.Itoa(reqBody.Len()))

	//Issue the request and get the bearer token from the JSON you get back
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err, resp)
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err, respBody)
	}

	type BearerToken struct {
		AccessToken string `json:"access_token"`
	}
	var b BearerToken
	json.Unmarshal(respBody, &b)

	//choose your API endpoint that supports application only auth context
	//and create a request object with that
	twitterEndPoint := "https://api.twitter.com/1.1/search/tweets.json?q=golang"
	req, err = http.NewRequest("GET", twitterEndPoint, nil)
	if err != nil {
		log.Fatal(err)
	}

	//Step 3: Authenticate API requests with the bearer token
	//include an Authorization header formatted as
	//Bearer <bearer token value from step 2>
	req.Header.Add("Authorization",
		fmt.Sprintf("Bearer %s", b.AccessToken))

	//Issue the request and get the JSON API response
	resp, err = client.Do(req)
	if err != nil {
		log.Fatal(err, resp)
	}
	defer resp.Body.Close()
	respBody, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return respBody
}
